import React from 'react';

const Lollipop = function(props) {
  return (
    <span className="lollipop" role="img" aria-label="Lollipop">🍭</span>
  );
}

export default Lollipop;